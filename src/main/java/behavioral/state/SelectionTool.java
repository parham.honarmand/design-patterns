package behavioral.state;

public class SelectionTool implements Tool {
    public void mouseDown() {
        System.out.println("selection icon");
    }

    public void mouseUp() {
        System.out.println("Draw a dashed rectangle");
    }
}
