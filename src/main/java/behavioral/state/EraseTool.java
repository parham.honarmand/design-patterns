package behavioral.state;

public class EraseTool implements Tool {
    public void mouseDown() {
        System.out.println("Erase icon");
    }

    public void mouseUp() {
        System.out.println("Erase something");
    }
}
