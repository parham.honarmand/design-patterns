package behavioral.memento;

import lombok.*;

//originator
@Setter
@Getter
@ToString
public class Editor {
    private String content;
    private String fontName;
    private float fontSize;

    public EditorState createState() {
        return new EditorState(content, fontName, fontSize);
    }

    public void restore(EditorState state) {
        content = state.getContent();
        fontName = state.getFontName();
        fontSize = state.getFontSize();
    }
}
