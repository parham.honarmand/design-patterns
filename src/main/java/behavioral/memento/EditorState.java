package behavioral.memento;

import lombok.*;

//behavioral.memento
@RequiredArgsConstructor
@Getter
public class EditorState {
    private final String content;
    private final String fontName;
    private final float fontSize;
}
