package behavioral.memento;

public class Main {
    public static void main(String[] args) {
        Editor editor = new Editor();

        //used to save history of editor
        History history = new History();

        editor.setContent("first content");
        editor.setFontName("first font");
        editor.setFontSize(1);

        history.push(editor.createState());

        editor.setContent("second content");
        editor.setFontSize(2);

        history.push(editor.createState());

        editor.setContent("third content");
        editor.setFontName("third font name");
        editor.setFontSize(3);

        //Editor(content=third content, fontName=third font name, fontSize=3.0)
        System.out.println(editor);

        editor.restore(history.pop());

        //Editor(content=second content, fontName=first font, fontSize=2.0)
        System.out.println(editor);

        editor.restore(history.pop());

        //Editor(content=first content, fontName=first font, fontSize=1.0)
        System.out.println(editor);
    }
}
