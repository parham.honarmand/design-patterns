package behavioral.memento;

import java.util.Stack;

//caretaker
public class History {
    Stack<EditorState> editorStates = new Stack<EditorState>();

    public void push(EditorState state) {
        editorStates.push(state);
    }

    public EditorState pop() {
        return editorStates.pop();
    }
}
