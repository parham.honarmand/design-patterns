package behavioral.strategy;

public class Main {
    public static void main(String[] args) {
        ImageStorage imageStorage = new ImageStorage();

        imageStorage.store("abc", new JpegCompressor(), new BlackAndWhiteFilter());

        imageStorage.store("abc", new PngCompressor(), new BlackAndWhiteFilter());
    }
}
