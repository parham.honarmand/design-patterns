package behavioral.iterator;

import lombok.RequiredArgsConstructor;

import java.util.Stack;

public class BrowsHistory<T> {
    private Stack<T> urls = new Stack<>();

    public void push(T url) {
        urls.push(url);
    }

    public T pop() {
        return urls.pop();
    }

    public Iterator<T> createIterator() {
        return new StackIterator(this);
    }

    //create this as inner class to access private urls iterable
    @RequiredArgsConstructor
    private class StackIterator implements Iterator<T> {
        private final BrowsHistory<T> history;
        private int index;

        @Override
        public Boolean hasNext() {
            return index < history.urls.size();
        }

        @Override
        public T getCurrent() {
            return history.urls.get(index);
        }

        @Override
        public void next() {
            index++;
        }
    }
}
