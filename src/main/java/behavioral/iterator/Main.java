package behavioral.iterator;

public class Main {
    public static void main(String[] args) {
        BrowsHistory<String> history = new BrowsHistory<>();

        history.push("a");
        history.push("b");
        history.push("c");
        history.push("d");

        Iterator<String> iterator = history.createIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.getCurrent());
            iterator.next();
        }
    }
}
